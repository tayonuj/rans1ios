//
//  ViewController.swift
//  Rans1
//
//  Created by Tayon UJ on 10/20/20.
//

import UIKit
import WebKit
class ViewController: UIViewController {
    
    @IBOutlet var webview: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webview.load(URLRequest(url: URL(string: "https://rans.health")!))
    }

    

}

